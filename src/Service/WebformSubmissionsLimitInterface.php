<?php

namespace Drupal\webform_submissions_limit\Service;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\webform\Utility\WebformElementHelper;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;

interface WebformSubmissionsLimitInterface {

  /**
   * @param string $ip : optional
   * @param array $data : [ 'WEBFORM_ID.FIELD_NAME' => $value ]
   * @param int $dateInterval (in seconds)
   * @param string $onlyWebform : optional
   * @return int
   */
  public function getSubmissionsCountBasedOnData(?string $ip = null, array $data = [], int $dateInterval = 0, ?string $onlyWebform = null): int;

  /**
   * Add validation protection
   * @param array $form
   * @param FormStateInterface $form_state
   * @param WebformSubmissionInterface $webform_submission
   */
  public function addFormProtection(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission);

  /**
   * Callback for validation
   * @param array $form
   * @param FormStateInterface $form_state
   * @param WebformSubmissionInterface $webform_submission
   */
  public function validateForm(array &$form, FormStateInterface $form_state, WebformSubmissionInterface $webform_submission);
}
