<?php

namespace Drupal\webform_submissions_limit\Service;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\webform\Plugin\WebformElementManagerInterface;
use Drupal\webform\Utility\WebformDateHelper;
use Drupal\webform\Utility\WebformElementHelper;
use Drupal\webform\WebformInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformThirdPartySettingsManagerInterface;

class WebformSubmissionsLimit {

  /**
   * The webform element plugin manager.
   *
   * @var \Drupal\webform\Plugin\WebformElementManagerInterface
   */
  protected $elementManager;

  /**
   * The webform third_party_settings manager.
   *
   * @var \Drupal\webform\WebformThirdPartySettingsManagerInterface
   */
  protected $webformThirdPartySettingsManager;

  /**
   * The database service.
   *
   * @var Connection
   */
  protected $database;

  /**
   * Constructs a new TOSearchManager object.
   */
  public function __construct(Connection $database, WebformElementManagerInterface $element_manager, WebformThirdPartySettingsManagerInterface $webform_third_party_settings_manager) {
    $this->database = $database;
    $this->elementManager = $element_manager;
    $this->webformThirdPartySettingsManager = $webform_third_party_settings_manager;
  }

  /**
   * @return bool: true if limit is count globally (all submissions across webforms) or false if only on current submitted webform
   */
  public function useGlobalLimit(): bool {
    return true;
  }

  /**
   * @return int
   */
  public function getLimitCount(): int {
    return 15;
  }

  /**
   * @return int: dateInterval (in seconds)
   */
  public function getLimitInterval(): int {
    return 3600;
  }

  /**
   * @param string $ip : optional
   * @param array $data : [ 'WEBFORM_ID.FIELD_NAME' => $value ]
   * @param int $dateInterval (in seconds)
   * @return int
   */
  public function getSubmissionsCountBasedOnData(?string $ip = null, array $data = [], int $dateInterval = 0): int {
    if (!(!empty($data) && $dateInterval > 0)) {
      return 0;
    }
    $query = $this->database->select('webform_submission_data', 'wsd')
      ->fields('wsd', array('sid'));
    $joinAlias = $query->leftJoin('webform_submission', 'ws', 'ws.sid = wsd.sid');
    $filtering = $query->orConditionGroup();
    // Data filtering
    if (!empty($data)) {
      $dataOr = $query->orConditionGroup();
      foreach ($data as $webform_str => $value) {
        [$webform_id, $webform_field] = explode('.', $webform_str);
        $and = $query->andConditionGroup()
          ->condition('wsd.name', $webform_field, '=')
          ->condition('wsd.value', $value, '=');
        if ($webform_id !== '_all_') {
          $and->condition('wsd.webform_id', $webform_id);
        }
        $dataOr->condition($and);
      }
      $filtering->condition($dataOr);
    }
    // IP filtering
    if ($ip) {
      $filtering->condition($joinAlias . '.remote_addr', $ip, '=');
    }
    // Date filtering
    if ($dateInterval > 0) {
      $query->condition($joinAlias . '.created', \Drupal::time()->getCurrentTime() - $dateInterval, '>=');
    }
    $query->condition($filtering);
    $query->groupBy('sid');
    // dump($query->__toString(), $query->getArguments());
    $executed = $query->execute();
    $results = $executed->fetchAll(\PDO::FETCH_ASSOC);
    return count($results);
  }

  /**
   * @param WebformInterface|null $webform
   * @param WebformSubmissionInterface|null $webform_submission
   * @return MarkupInterface
   */
  protected function getLimitReachedErrorMessage(?WebformInterface $webform = null, ?WebformSubmissionInterface $webform_submission = null): MarkupInterface {
    $globalErrorMessage = (string) $this->webformThirdPartySettingsManager->getThirdPartySetting('webform_submissions_limit', 'global_limit_error_message');
    if ($webform && $webform_submission) {
      $specificErrorMessage = (string) $webform->getThirdPartySetting('webform_submissions_limit', 'global_limit_error_message');
      if ($specificErrorMessage) {
        return Markup::create($specificErrorMessage);
      }
    }
    return $globalErrorMessage ? Markup::create($globalErrorMessage) : new TranslatableMarkup('You have reached the limit for this form.');
  }

  /**
   * {@inheritdoc}
   */
  public function addFormProtection(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
    $webform_submission = $form_state->getFormObject()->getEntity();
    if (!$webform_submission->isCompleted()) {
      $form['#validate'][] = [$this, 'validateForm'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\webform\WebformSubmissionInterface $webform_submission */
    $webform_submission = $form_state->getFormObject()->getEntity();
    $webform = $webform_submission->getWebform();

    $ip = null; // $webform_submission->get('remote_addr')->value ?? null;

    $globalLimit = (int) $this->webformThirdPartySettingsManager->getThirdPartySetting('webform_submissions_limit', 'global_limit');
    $globalLimitDataFields = (string) $this->webformThirdPartySettingsManager->getThirdPartySetting('webform_submissions_limit', 'global_limit_data_fields');
    $globalLimitNumber = (int) $this->webformThirdPartySettingsManager->getThirdPartySetting('webform_submissions_limit', 'global_limit_number') ?: 1;
    $globalLimitTime = (int) $this->webformThirdPartySettingsManager->getThirdPartySetting('webform_submissions_limit', 'global_limit_time') ?: 0;
    $globalLimitTimeUnit = (int) $this->webformThirdPartySettingsManager->getThirdPartySetting('webform_submissions_limit', 'global_limit_time_unit') ?: 3600;
    $globalLimitTimeFrame = $globalLimitTime * $globalLimitTimeUnit;
    $globalFieldNames = array_filter(array_map('trim', explode(',', str_replace(' ', ',', $globalLimitDataFields))));

    $specificLimit = (int) $webform->getThirdPartySetting('webform_submissions_limit', 'global_limit');
    $specificLimitDataFields = (string) $webform->getThirdPartySetting('webform_submissions_limit', 'global_limit_data_fields');
    $specificFieldNames = array_filter(array_map('trim', explode(',', str_replace(' ', ',', $specificLimitDataFields)))) ?: $globalFieldNames;

    $errorField = null;
    $errorLimit = false;
    $errorLimitMessage = null;

    // Validates this webform
    if ($globalLimit === WEBFORM_SUBMISSIONS_LIMIT_ENABLED_ALL || !!$specificLimit) {
      if (!empty($specificFieldNames)) {
        $specificLimitNumber = (int) $webform->getThirdPartySetting('webform_submissions_limit', 'global_limit_number') ?: $globalLimitNumber;
        $specificLimitTime = (int) $webform->getThirdPartySetting('webform_submissions_limit', 'global_limit_time') ?: $globalLimitTime;
        $specificLimitTimeUnit = (int) $webform->getThirdPartySetting('webform_submissions_limit', 'global_limit_time_unit') ?: $globalLimitTimeUnit;
        $specificLimitTimeFrame = $specificLimitTime * $specificLimitTimeUnit;
        if ($specificLimitNumber > 0) {
          $data = [];
          foreach ($specificFieldNames as $emailFieldName) {
            if ($emailFieldName && $webform_submission->getElementData($emailFieldName)) {
              $data[sprintf('%s.%s', $webform->id(), $emailFieldName)] = $webform_submission->getElementData($emailFieldName);
              $errorField = $emailFieldName;
            }
          }
          $count = $this->getSubmissionsCountBasedOnData($ip, $data, $specificLimitTimeFrame);
          if ($count >= $specificLimitNumber) {
            $errorLimit = true;
            $errorLimitMessage = $this->getLimitReachedErrorMessage($webform, $webform_submission);
          }
          // dump([
          //   'count' => $count,
          //   'specificLimitNumber' => $specificLimitNumber,
          //   'specificLimitTimeFrame' => $specificLimitTimeFrame,
          //   'specificFieldNames' => $specificFieldNames,
          //   'data' => $data,
          // ]);
        }
      }
    }
    // Validates global limit (if not already in error for this webform constraints)
    if (!$errorLimit && $globalLimit === WEBFORM_SUBMISSIONS_LIMIT_ENABLED_ALL) {
      if (!empty($globalFieldNames) && $globalLimitNumber > 0) {
        $thisValues = [];
        foreach ($specificFieldNames as $emailFieldName) {
          if ($emailFieldName && $webform_submission->getElementData($emailFieldName)) {
            $thisValues[] = $webform_submission->getElementData($emailFieldName);
            $errorField = $emailFieldName;
          }
        }
        $data = [];
        foreach ($globalFieldNames as $emailFieldName) {
          foreach ($thisValues as $thisValue) {
            $data[sprintf('_all_.%s', $emailFieldName)] = $thisValue;
          }
        }
        $count = $this->getSubmissionsCountBasedOnData($ip, $data, $globalLimitTimeFrame);
        if ($count >= $globalLimitNumber) {
          $errorLimit = true;
          // dump([
          //   'count' => $count,
          //   'globalLimitNumber' => $globalLimitNumber,
          //   'globalLimitTimeFrame' => $globalLimitTimeFrame,
          //   'globalFieldNames' => $globalFieldNames,
          //   'data' => $data,
          // ]);
        }
      }
    }
    // Set error
    if ($errorLimit) {
      $errorLimit = true;
      $element = $form;
      if ($errorField) {
        $element =& WebformElementHelper::getElement($form, $errorField);
      }
      $form_state->setError($element, $errorLimitMessage ?: $this->getLimitReachedErrorMessage());
      // dump($errorField, $errorLimitMessage, $form_state); exit;
    }
    // exit;
  }
}
